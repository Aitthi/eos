// these variables need to be defined in the global scope
// so renderIcons can access them instead of the ones defined in
// the controller
const getIconsService = sinon.fake()
const getMoreInfoFromIconService = sinon.fake()

describe("Eos icons", () => {

  describe('renderIcons', () => {
    const iconsWrap = $('<div class="js-icons-container"><h3 class="js-icons-category-name"></h1><div class="js-eos-icons-list"><div class="js-icon-display"><div class="js-eos-icons"><strong class="js-icon-name"></strong></div></div><div class="js-animated-icon-display"><img class="js-animated-eos-icons"><strong class="js-icon-name"></strong></div></div></div>')
    const collection = [{"name":"action_chains","category": "virtualization","type": "static"},{"name":"activate_subscriptions","category": "virtualization","type": "static"},{"name":"loading","category": "virtualization","type": "animated"}]
    const emptyCollection = []

    before( (done) => {
      $('body').prepend(iconsWrap)
      $iconsContainer = $('.js-icons-container')
      done()
    })

    after( (done) => {
      $(iconsWrap).remove()
      done()
    })

    it('Should print the collection of 2 static icons and 1 animated icon', () => {
      _iconsContainerList = $('.js-eos-icons-list').clone(true)
      $('.js-eos-icons-list').remove()
      $categoryContainer = $('.js-icons-category-name').clone(true)
      $('.js-icons-category-name').remove()
      
      $animatedIconDisplayTemplate = $($newIconsContainerList).find('.js-animated-icon-display')
      $iconDisplayTemplate = $($newIconsContainerList).find('.js-icon-display')
      $($newIconsContainerList).find('.js-animated-icon-display').remove()
      $($newIconsContainerList).find('.js-icon-display').remove()

      renderIcons(collection)

      expect($('.js-icons-category-name')).to.contain('Virtualization')
      expect($('.js-icon-display')).to.have.lengthOf(2)
      expect($('.js-icon-display')).to.contain('action_chains')
      expect($('.js-icon-display')).to.contain('activate_subscriptions')
      expect($('.js-animated-icon-display')).to.have.lengthOf(1)
      expect($('.js-animated-icon-display')).to.contain('loading')
    })

    it('Should not print any icons when collection is empty', () => {
      $iconDisplayTemplate = $('.js-icon-display').clone(true)
      $('.js-icon-display').remove()

      $animatedIconDisplayTemplate = $('.js-animated-icon-display').clone(true)
      $('.js-animated-icon-display').remove()
      renderIcons(emptyCollection)
      
      expect($('.js-icon-display')).to.have.lengthOf(0)
      expect($('.js-animated-icon-display')).to.have.lengthOf(0)
    })
  })

  describe('On search', () => {
    afterEach(() => {
      sinon.restore()
    });

    it('Should reset the list of icons when the search is empty', () => {
      // create spies to track methods called
      const spy = sinon.spy(window, 'getIconsCollection')
      // make an empty search
      onSearch()
      expect(spy).called
    })

    it('Should filter the list of icons when the search is not empty', () => {
      // create spies to track methods called
      const spy = sinon.spy(window, 'getIconsCollection')
      // make search with argument
      onSearch('something')
      expect(spy).to.have.been.calledWith('something')
    })
  })

})
