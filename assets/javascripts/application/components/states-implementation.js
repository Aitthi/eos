let $eleDisplayTemplate
$(function () {
  Prism.highlightAll() // eslint-disable-line no-undef
  const type = $('.js-element-id').attr('name')
  // Prepare li to show element states radio buttons
  $eleDisplayTemplate = $('.js-state-current').clone(true)
  $('.js-state-current').remove()
  getElementStates(type)
})

const renderElementsStates = (elementStates, type) => {
  const elementCollection = elementStates.filter(ele => ele.type === `${type}`).map(ele => ele.states)
  for (let i = 0; i < elementCollection[0].length; i++) {
    const eleState = elementCollection[0][i].status

    const newEleDisplay = $eleDisplayTemplate.clone(true)

    // Make first letter uppercase and add space between words
    const labelText = eleState.substr(0, 1).toUpperCase() + eleState.substr(1).split('-').join(' ')

    // Add id value to input field
    $(newEleDisplay).find('.js-element-id').attr({ 'id': `${eleState}-state`, 'value': `${eleState}` })
    $(newEleDisplay).find('.js-element-label').attr('for', `${eleState}-state`).text(`${labelText}`)
    $('.js-states-list').append(newEleDisplay)
  }

  // Add checked attibute to default state on page load
  $('#default-state').prop('checked', true)

  // Add default text to How to use and code container on page load
  updateElementState(elementCollection[0], 'default')

  // Change text in How to use and code container on click
  $('.js-state-current input').click(function () {
    const state = $(this).attr('value')

    // Update states info as per the states
    updateElementState(elementCollection[0], state)
  })
}

const updateElementState = (elementCollection, state) => {
  const elementStateData = elementCollection.filter(ele => ele.status === `${state}`)

  $('.js-how-to-use').text(elementStateData[0].how_to_use)
  let newCode = ''
  for (let i = 0; i < elementStateData[0].code.length; i++) {
    newCode += `${elementStateData[0].code[i]} 
`
  }
  $('.js-example-inner-box').html(newCode)
  $('.js-snippet-code code').text(newCode)
  Prism.highlightAll() // eslint-disable-line no-undef
}

function getElementStates (type) {
  getElementsStatesService(result => { // eslint-disable-line no-undef
    const elementStates = result

    renderElementsStates(elementStates, type)
  })
}
