$(document).ready(function () {
  showMoreResources()

  $(window).resize(function () {
    showMoreResources()
  })
})

const showMoreResources = () => {
  // Toggle between 3 or 2 elements to display based on windows size.
  const showManyElements = window.innerWidth > 1090 ? 3 : 2

  $('.js-resource-content').slice(0, showManyElements).show()
  $('.js-load-more').on('click', (e) => {
    e.preventDefault()
    $('.js-resource-content:hidden').slice(0, showManyElements * 2).slideDown()
    if ($('.js-resource-content:hidden').length === 0) {
      $('.js-load-more').fadeOut('slow')
    }
  })
}
