const { strapiGraphql } = require('./strapi-graphql-query')
const axios = require('axios');

const authMiddleware = async (req, res, next) => {
  try {
    /* It will fetch the application requiresAuth property from Strapi */
    const { data } = await strapiGraphql('applications{requiresAuth}')
    const { requiresAuth } = data.applications[0]

    /* If false, move on and don't ask the user for credentials. */
    if (!requiresAuth) return next()

    /* If false, auth the user and check the cookie with the JWT */
    return await axios.get(`${process.env.EOS_STRAPI_SERVER_DEV}/users/me`, {
      headers: {
        Authorization: `Bearer ${req.cookies.token}`,
      }
    })
      .then(_ => {
        /* If token is correct, move one. */
        next()
      })
  } catch (error) {
    /* If the token is not correct and you're not on the auth page, it will redirect you login form. The frontend will take care of creating a new token if your credentials are correct.  */
    if (req.originalUrl !== '/auth') {
      /* We need to let express know that when the uses clicks on login, don't use the logic to check for tokens and allow the request to the api/auth */
      return req.originalUrl === '/api/auth'
        ? next()
        : res.redirect('/auth')
    } else {
      next()
    }
  }
}

module.exports = {
  authMiddleware
}
